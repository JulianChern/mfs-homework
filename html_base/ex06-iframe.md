## 什么是 iframe?
iframe 是html的一个标签，iframe 会创建包含另外一个文档的内联框架。

## 如何在当前页面中嵌入其他页面？

示例如下：
```html
<!DOCTYPE html>
<html>
    <head>
        <title>ex06-iframe</title>
        <meta charset="utf-8">
    </head>
    <body>
        <!-- 其他内容 -->
        <iframe name="iframe1" src="https://www.baidu.com" width="800px" height="800px" frameborder="0"></iframe>
        <!-- 其他内容 -->
    </body>
</html>
```

## iframe 有哪些常见应用？

* 插入广告
* 实现简单的局部刷新

## 如何使用 &lt;a> 标签控制 iframe 中显示的内容？

使用&lt;a>标签的target属性，与iframe的name属性，两者值相等时，则&lt;a>标签可以控制iframe中的显示内容。

## 为什么越来越少的人使用 iframe？

从其常用场景看：

通过iframe设计的广告很容易被屏蔽，因而广告渐渐不采用iframe设计。

通过iframe设计的局部刷新，会增加额外请求，并且js实现的局部刷新能比iframe有更好的效果。

## frame 和 iframe 有什么区别？

* frame不能脱离frameSet单独使用，iframe可以。
* frame不能放在body中,否则不能正常显示。
* frame的高度只能通过frameSet控制；iframe可以自己控制，不能通过frameSet控制。

## 请实现一个类似 gitbook 的左边点击右边局部刷新的效果

```html
<!DOCTYPE html>
<html>

<head>
    <title>ex06-homework</title>
    <meta charset="utf-8">
    <style>
        .left {
            position: absolute;
            left:0;
            width: 300px;
            height: 100%;
            font-size:20px;
            top:50%;
            text-align:center;
        }
        .right{
            border-left: 1px solid #aaa;
            position: absolute;
            left: 300px;
            width: calc(100%-300px);
            height: 100%;
            right:0;
        }
    </style>
</head>

<body>
    <div class="left">
        <div>
            <a href="https://baidu.com" target="right_frame">百度</a></div>
        <div>
            <a href="https://www.sina.com.cn" target="right_frame">新浪</a>
        </div>
    </div>
    <div class="right">
        <iframe src="https://baidu.com" name="right_frame" width="100%" height="100%" frameborder="0"/>
    </div>
</body>

</html>
```