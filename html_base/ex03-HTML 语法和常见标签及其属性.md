## HTML 元素需要正确嵌套，请给出示例代码说明何为正确嵌套，何为不正确嵌套?

正确嵌套示例：
```html
<p>
    <span>正确嵌套</span>
</p>
```

错误嵌套示例：
```html
<p>
    <span>
</p>
    </span>
```

## HTML 元素属性是什么？在哪里如何指定属性？常见属性有哪些？

HTML元素属性是用来为元素添加附加信息， 一般在开始标签中，并且一般以名称/值对的形式出现。

在开始标签中指定属性。

常见属性有：

|属性|描述|
|:--|:--|
|class|为html元素定义一个或多个类名（classname）(类名从样式文件引入)|
|id|定义元素的唯一id|
|style|规定元素的行内样式（inline style）|
|title|描述了元素的额外信息 (作为工具条使用)|

## 空标签如何自闭和？

在开始标签中关闭，如`<p />`、`<br />`

## HTML 文档中多个空白字符如何显示？请自行查阅资料，实现显示多个空格

使用转义字符来显示多个空格，实例如下：
```html
<p>这是一个     段落</p>
<p>这是一个&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;段落</p>
```

下面为演示：

<p>这是一个     段落</p>
<p>这是一个&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;段落</p>

## HTML 中字符转义是什么意思？哪些字符需要转义？

HTML中<，>，&等有特殊含义（<，>，用于链接签，&用于转义），不能直接使用，若要显示则需要通过转义。

下面是常用的需要转义的字符：


|显示|说明|实体名称|
|:--|:--|:--|
|&ensp;|半方大的空白|\&ensp;|
|&emsp;|全方大的空白|\&emsp;|
|&nbsp;|不断行的空白格|\&nbsp;|
|<|小于|\&lt;|
|>|大于|\&gt;|
|&|&符号|\&amp;|
|"|双引号|\&quot;|
|©|版权|\&copy;|
|®|已注册商标|\&reg;|

## 有哪些常见标签？请给出代码示例(Demo)

```html
<!--标题-->
<h1>h1</h1>
<h2>h2</h2>
<h3>h3</h3>
<h4>h4</h4>
<h5>h5</h5>
<h6>h6</h6>
<!-- 段落和块 -->
<p>Text</p>
<div>Text</div>
<!-- 行内书写 -->
<span>Text</span>
<!-- 超链接 -->
<a href="http://www.w3school.com.cn" target="_blank">This is a link</a>
<!-- 图片 -->
<img src="a.jpg" width="100" height="10" alt="alt"/>
<!-- 有序列表 -->
<ol>
  <li>a</li>
  <li>b</li>
  <li>c</li>
</ol>
<!-- 无序列表 -->
<ul>
  <li>Coffee</li>
  <li>Tea</li>
  <li>Milk</li>
</ul>
<!-- 换行 -->
<br/>
<!-- 分割线 -->
<hr/>
<!-- 按钮 -->
<button>按钮</button>
<!-- 表 -->
<table>
    <thead>
        <tr>
            <th>姓名</th>
            <th>成绩</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>张三</td>
            <td>100</td>
        </tr>
        <tr>
            <td>李四</td>
            <td>90</td>
        </tr>
        <tr>
            <td>王五</td>
            <td>60</td>
        </tr>
    </tbody>
</table>
```

## 默认样式下\<p>和\<div>有什么区别？

* div就是普通的块标签，多用于布局；p是语义化的段落标签，用于文章分段
* div默认没有边距，p标签为了表示文章分段，有默认的间距

## 请自行查阅资料，学习行内元素和块状元素的区别

|类型|行内元素|块级元素|
|:--|:--|:--|
|默认布局方向|水平布局|垂直布局|
|盒模型特性|无（image和input除外）|可以设置width、height、padding、margin、默认为父容器大小|
|内部元素|行内元素、文本|行内元素、文本、块级布局|
|相互装换|display:block|display:inline;|

注意：img和input是比较特殊的行内元素可以设置width和height，因为他们是替换行内元素，可以理解成inline-block

此处参考：

<https://blog.csdn.net/u012436704/article/details/83503206>

<https://www.cnblogs.com/yanqiu/p/8987126.html>