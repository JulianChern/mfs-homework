## 什么是 HTTP 协议？HTTP 协议常见的请求类型有哪些？

HTTP协议（HyperText Transfer Protocol，超文本传输协议）是因特网上应用最为广泛的一种网络传输协议，所有的WWW文件都必须遵守这个标准。

HTTP是一个基于TCP/IP通信协议来传递数据（HTML 文件, 图片文件, 查询结果等）。

常见的请求类型有GET、POST、PUT以及DELETE等。

## HTTP 请求和回复的关系是什么？

GET请求的回复是资源对象信息。

POST请求的回复是新生成的资源对象信息。

PUT请求的回复是更新后的资源对象信息。

DELETE请求的回复是删除资源对象后的信息。

## GET 请求一般是安全的和幂等的，请解释这句话的具体含义

安全：指GET请求只获取而不会修改后台数据。

幂等：指对同一URL的多个GET请求，返回的是同样的结果

## POST 请求一般用于干什么？它是幂等的吗？

POST请求一般用于向服务器新增或修改数据。

POST请求不幂等，多次POST请求会创建多份资源。


## GET 和 POST 请求有什么区别？

GET请求一般用于获取/查询资源信息，POST请求则用于新增/修改资源信息。

GET请求的URL会被放在浏览器历史和WEB 服务器日志里面。 POST请求发送的信息放在请求体里，发完就没有了。后者安全一点。

## 什么时候用 GET 请求，什么时候用 POST 请求？

GET请求使用场景：

* 获取服务器上的资源
* 请求中的URL可以被手动输入
* 请求中的URL可以被存在书签里，或者历史里，或者快速拨号里面，或者分享给别人。
* 请求中的URL是可以被搜索引擎收录的。
* 请求中的URL可以被缓存。

POST请求使用场景：

* 希望修改（增加）服务器上的资源
* 不希望提交的信息被在 URL 中看到
* 提交的信息比较大
* 提交的有文件，或者二进制内容

## GET 请求提交表单是如何编码的？编码后的数据放在哪里？

GET请求提交表单是将参数转为网络编码（%byte1%byte2%byte3），也就是用%分割的UTF-8编码，然后保存在请求的URL尾部。

## POST 请求提交表单是如何编码的？编码后的数据放在哪里？

POST请求提交表单是默认将参数转为utf-8编码，然后放在请求数据包的消息体中。

## HTML5新提供的表单控件有哪些？

新的属性有placeholder、autofocus

新的控件有datalist以及input的新特性——type为number,url,date,time,color等。

## HTML5提供了哪些表单验证方法？

* 利用type验证，如在input标签的type属性设置为url，那么输入框只接受url
* 利用min/max属性验证，将input标签的type设置为number，可以通过min、max属性来限制输入的数字范围。
* 利用正则表达式，在input标签的pattern属性可设定正则表达式，如[A-Za-z]{3}表示只接受3个字母的输入。
* required属性，表示该输入框必须输入内容。

## 代码题

```html
<!DOCTYPE html>
<html>
    <head>
        <title>ex05-表单二</title>
        <meta charset="utf-8">
        <meta name="author" content="czm">
        <meta name="keywords" content="hw">
    </head>
    <body>
        <form action="siginup" method="POST">
            <div>邮箱：<input type="email" name="mail" required /></div>
            <div>数字：<input type="number" name="number" min="0" max="100" step="2" /></div>
            <div>城市：
                <input list="location" name="location" />
                <datalist id="location">
                    <option>北京</option>
                    <option>上海</option>
                    <option>广州</option>
                    <option>深圳</option>
                </datalist>
            </div>
            <div>
                <button type="submit">提交</button>
                <button type="reset">复位</button>
            </div>
        </form>
    </body>
</html>
```