## HTML是什么?

HTML全称：HyperText Markup Language，译为超文本标记语言，其含义如下：

* 超文本：超级文本的中文缩写。超链接的方法，将各种不同空间的文字信息组织在一起。可以理解为“超越纯文本”
* 标记语言：不是编程语言，而且使用一系列的标记，来表示/存储信息

## 什么是 HTML 标签？它有什么特性？

HTML标签是由尖括号包围的关键词，比如 \<html>

HTML标签通常是成对出现的，比如 \<b> 和 \</b>
标签对中的第一个标签是开始标签，第二个标签是结束标签

## 有哪些常见浏览器和其对应的内核？

|渲染引擎|浏览器|
|:--|:--|
|Trident|IE|
|Gecko|Firefox|
|Webkit|Chrome,Safari, Apple & Android mobile devices|
|Presto|Opera, Opera mini|

## \<!DOCTYPE>声明是什么？

该声明指明了当前HTML文档的版本信息。其非HTML标签，并且必须是HTML文档的第一行。

## \<html>，\<body>，\<head>标签的相对包含关系是什么？

\<html>是根标签，即最外层的标签，有且只有一个。

\<body>、\<head>都是\<html>的直接子元素。即不能存在的标签xxx，它既是\<head>的父元素，又是\<html>的子元素。

\<body>、\<head>两者是同级的元素。

## \<head>标签中可以放置哪些子标签？各有什么作用？

可放置如下标签
* \<title>标签，其定义了不同文档的标题,在 HTML/XHTML 文档中是必须的,浏览器工具栏的标题。
* \<base>标签描述了基本的链接地址/链接目标，该标签作为HTML文档中所有的链接标签的默认链接。
* \<link>标签定义了文档与外部资源之间的关系,通常用于链接到样式表
* \<style>标签定义了HTML文档的样式文件引用地址,指定样式文件来渲染HTML文档
* \<meta>标签提供了元数据.元数据也不显示在页面上，但会被浏览器解析。META元素通常用于指定网页的描述，关键词，文件的最后修改时间，作者，和其他元数据。元数据可以使用于浏览器（如何显示内容或重新加载页面），搜索引擎（关键词），或其他Web服务。一般放置于\<head>区域，常用属性如下：
    * \<meta charset="utf-8">定义文档的字符集
    * \<meta name="keywords" content="HTML, CSS, JavaScript">为网页定义描述内容
    * \<meta name="author" content="mafengshe">定义网页作者
    * \<meta http-equiv="refresh" content="30">定时刷新当前页面（30秒）

* \<script>标签用于加载脚本文件，如： JavaScript。在标签内部书写脚本或引入脚本文件位置

ex02-HTML结构.html <https://github.com/zfhxi/mfs-homework/blob/master/ex02-HTML%E7%BB%93%E6%9E%84.html>
