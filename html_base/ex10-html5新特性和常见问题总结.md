## HTML5引入了哪些新标签？

* canvas标签：用于绘画
* svg标签：可伸缩矢量图形
* video、audio标签：用于媒介播放
* 语义化标签如 article、footer、header、nav、section
* 新的表单控件，比如 calendar、date、time、email、url、search

## Canvas是什么？它能干什么？

canvas是使用 JavaScript 在网页上绘制图像的标签。

canvas 拥有多种绘制路径、矩形、圆形、字符以及添加图像的方法。

## SVG 是什么？它能干什么？它和 Canvas 有什么区别？

**SVG是什么？**

* SVG 指可伸缩矢量图形 (Scalable Vector Graphics)
* SVG 用于定义用于网络的基于矢量的图形
* SVG 使用 XML 格式定义图形
* SVG 图像在放大或改变尺寸的情况下其图形质量不会有损失
* SVG 是万维网联盟的标准

**SVG可以干什么？**

SVG可以画可缩放的矢量图，并且图像质量不随缩放大小改变。

SVG 图像可在任何的分辨率下被高质量地打印


**和canvas区别**

* svg是用来画静态图，矢量图的，可以做图标
* canvas可以画动态图，粒子图，3d图。

## 如何在网页上播放视频、音频？

示例：
```html
<video width="320" height="240" controls>
    <source
        src="http://static.mafengshe.com/video/2017-05-13-%E6%AF%8D%E4%BA%B2%E8%8A%82&520%E4%B8%93%E9%A2%98-%E5%9B%BE%E7%89%87%E5%A2%99.mp4"
        type="video/mp4" />
    Your browser does not support the video tag.
</video>
<audio controls>
    <source src="https://static.mafengshe.com/audio/%E4%BD%A0%E8%BF%98%E8%A6%81%E6%88%91%E6%80%8E%E6%A0%B7.mp3"
        type="audio/mp3" />
    Your browser does not support this audio format.
</audio>
```

## HTML5引入了哪些结构性元素？他们各有哪些含义？

* article元素 - article元素代表文档、页面或应用程序中独立的、完整的、可以独自被外部引用的内容，与上下文不相关的独立内容
* section元素 - section元素表示页面中的一个内容区块，比如章节、页眉、页脚或页面中的其他部分 - 不要为没有标题的内容区块使用section元素
* nav元素 - 表示页面中导航链接的部分，例如：传统导航条，侧边栏导航，页内导航，翻页等
* aside元素 - aside元素表示article元素的内容之外的、与article元素的内容相关的辅助信息，它可以包含与当前页面或主要内容相关的引用、侧边栏、广告、导航条，以及其他类似的有别于主要内容的部分
* header元素 - 表示页面中一个内容区块或整个页面的标题
* hgroup元素 - 将标题及其子标题进行分组的元素。hgroup元素通常会将h1-h6元素进行分组，譬如一个内容区块的标题及其子标题算一组。
```html
<hgroup>
  <h1>文章主标题</h1>
  <h2>文章子标题</h2>
</hgroup>
```
* footer元素 - 表示整个页面或页面中一个内容区块的脚注。一般来说，它会包含创作者的姓名、创作日期以及创作者联系信息
* figure元素 - 表示一段独立的流内容，一般表示文档主体流内容中的一个独立单元。使用figcaption元素为figure元素组添加标题

## 如果网页发生乱码，我们应该检查什么？具体该怎么做？

检查网页文档编码和meta声明的charset是否一致。

请写出一个包含乱码的网页，并合理设置字符集编码，使得它可以正常显示

示例，使用GB 2312保存：
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    这是中文字符。
</body>
</html>
```
打开后显示如下：
```
���������ַ���
```
将charset改为`GB 2312`后，显示如下：
```
这是中文字符。
```


## 在哪里查浏览器对标签或属性的支持程度? 请查询&lt;video>标签的浏览器兼容性，并截图

上canius.com

![](https://work.mafengshe.com/static/upload/article/pic1567827656128.jpg)

