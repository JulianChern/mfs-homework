## 问答题
### 程序语言中有哪三种控制结构？请用文字描述，并画图示意。

顺序结构：程序从上往下执行语句。
选择结构：程序根据条件选择某一条路径执行。
循环结构：重复执行某一代码块，直至不满足条件。

示意图如下：
```txt
 +-----+                   +--------+                          +---------+
 |     |                   |        |                          |         |
 |     |                   |        |                          |         |
 +--+--+                   +--------+                          +----+----+
    |                    +----+  +-------+                          |
    |                    |               |                          +<-----------+
    v                    v               v                          v            |
+---+---+            +---+---+       +---+---+                 +----+-----+      |
|       |            |       |       |       |                 |          |      |
|       |            |       |       |       |                 |          |      |
+---+---+            +---+---+       +---+---+                 +----+-----+      |
    |                    |               |                          |            |
    |                    |               |                          |            |
    v                    |               |                          v            |
+---+---+                |  +--------+   |                     +----+------+     |
|       |                +->+        +<--+                     |           |     |
|       |                   |        |                         |           +-----+
+-------+                   +--------+                         +----+------+
                                                                    |
                                                                    |
                                                                    v
                                                               +----+-----+
                                                               |          |
                                                               |          |
                                                               +----------+

```


### if ... else ... 语句的含义是什么？

当if()内的条件为真执行if后的代码块，否则执行else后的代码块。

### 当 if 或是 else 子句只有一行代码时，我们可以省略代码块（{}）吗？如果可以省略我们应该通过什么办法使代码可读性增加

可省略，并且尽量换行并缩进

### if 语句支持嵌套吗？如何使用嵌套？

支持嵌套。
形如：
```js
if(条件1){
    ...
    if(条件2){
        ...
    }else{
        ...
    }
}
else{
    ...
    if(条件3){
        ...
    }else{
        ...
    }
}
```

### 对于比较复杂的嵌套我们可以写成如下形式，请谈谈为什么可以写成这种形式
```js
if()
  ...
else if()
  ...
else
  ...
```
这使用较少的代码量就实现了选择结构含有多个分支的情况，更加简洁，增强了代码可读性。

### 对于多个分支的分支语句我们可以用 switch 语句，switch 语句的结构是什么样的？
```js
switch(variable){
    case condition1:
        ...
        break;
    case condition2:
        ...
        break;
    case condition3:
        ...
        break;

    ...

    default:
        ...
}
```

这里的variable可以是字符串。
`default`如果是放在其他case之前，需要加break。

### 对于 switch 语句的每一个子句，我们需要使用 break 吗？为什么？

一般情况下需要，对于放在末尾的default子句可省略。
按照需求是，仅执行匹配的case下的代码块，执行完后跳出switch不再执行其他分支，因而break很有必要。
如果没有，他会执行每种case下的代码块，那么就失去了`switch`的意义。

### switch 语句可以使用哪种数据结构替代？请给出实例代码。

可以通过数组替代。
如：
```js
var d=new Date().getDay();
switch (d)
{
  case 0:
    console.log("今天是星期日")
    break;
  case 1:
    console.log("今天是星期一")
    break;
  case 2:
    console.log("今天是星期二")
    break;
  case 3:
    console.log("今天是星期三")
    break;
  case 4:
    console.log("今天是星期四")
    break;
  case 5:
    console.log("今天是星期五")
    break;
  case 6:
    console.log("今天是星期六")
    break;
}
```
通过数组替代后：
```js
var arr=["日","一","二","三","四","五","六"];
var d=0;
var todayIs="今天是星期"+arr[d];
console.log(todayIs);
```

### 对于需要多次执行的相同或者类似的代码，我们应该使用什么样的控制结构？它一共有几种形式？请给出示例代码。
循环结构。
有四种形式。

1.for循环：
```js
for(statement1;statement2;statement3){
    ...
}

```
2.for in:
```js
for(var x in iterateableObj){
    ...
}
```
3.while():
```js
while(statement){
    ...
}
```
4.do...while():
```js
do{

}while(statement);
```

### 对于 for(语句1; 语句2; 语句3) 语句，语句1，语句2, 语句3 含义是什么？他们各是在什么时候执行？

语句1为初始条件，在进入循环时执行且执行一次；
语句2为终止条件，每次循环都执行且在花括号的代码块之前执行；
语句3为初始条件中变量的变化规则，每次循环执行完花括号中的代码块后再执行。


### for(var i in obj) 含义是什么？如何使用？

`for(var i in obj)`用以遍历对象的所有属性。
如：
```js
var person = { fname: "John", lname: "Doe", age: 25 };
var txt = ""
for (x in person)  // x 为属性名
{
    txt += x + ":" + person[x] + "\n";
}
console.log(txt)
```

## 代码题

### 请填写如下函数，使得当 flag 为 true 时，函数返回 1；否则返回0
```js
function func(flag){
 // 请在这里填写代码

}
```
填写后：
```js
function func(flag){
    return flag?1:0;
}
```

### 请实现顺序遍历数组，发现数组元素为 5 时终止遍历。如数组 [0, 1, 2, 3, 4, 5, 6]，应该遍历 [0, 1, 2, 3, 4]

```js
var arr = [1, 2, 3, 4, 5, 6 ,7];
function returnIf5(arrObj) {
    var arrRes = [];
    for (var i in arrObj)
        if (5 != arrObj[i])
            arrRes.push(arrObj[i])
        else
            break;
    return arrRes;
}
//测试
var arrString = "";
var rArr=returnIf5(arr);
for (var i in rArr) {
    arrString += rArr[i] + " ";
}
console.log(arrString);//1 2 3 4
```

### 请实现顺序遍历数组，发现数组元素为 5 时跳过。如数组 [0, 1, 2, 3, 4, 5, 6]，应该遍历 [0, 1, 2, 3, 4, 6]

```js
var arr = [1, 2, 3, 4, 5, 6, 7];
function jumpIf5(arrObj) {
    var arrRes = [];
    for (var i in arrObj)
        if (5 != arrObj[i])
            arrRes.push(arrObj[i])
    return arrRes;
}
//测试
arrString = "";
var jArr=jumpIf5(arr);
for (var i in jArr) {
    arrString += jArr[i] + " ";
}
console.log(arrString);//1 2 3 4 6 7
```

### 已知数组 [0, 1, 2, 3, 4, 5, 6]，请实现代码将映射为 ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
```js
var iArr=[0,1,2,3,4,5,6];
var arr = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
for(var i in iArr){
    console.log("line "+iArr[i]+": 今天是"+arr[iArr[i]]);
}
```