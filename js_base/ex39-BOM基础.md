## 问答题

### BOM 是什么？提供的 API 让我们能操作什么？

**BOM概念**
BOM是Browser Object Model的缩写，简称浏览器对象模型
BOM提供了独立于内容而与浏览器窗口进行交互的对象
由于BOM主要用于管理窗口与窗口之间的通讯，因此其核心对象是window
BOM由一系列相关的对象构成，并且每个对象都提供了很多方法与属性

**BOM能做的事**
BOM提供了一些访问窗口对象的一些方法，我们可以用它来移动窗口位置，改变窗口大小，打开新窗口和关闭窗口，弹出对话框，进行导航以及获取客户的一些信息如：浏览器品牌版本，屏幕分辨率。
但BOM最强大的功能是它提供了一个访问HTML页面的一入口——document对象，以使得我们可以通过这个入口来使用DOM的强大功能。

![](http://bbs.static.mafengshe.com/FpsoK2Xo_gnAuLO248esVGJ8G9_N?imageMogr2/quality/40)

### window.name 有怎样的特性

属性用于设置当前浏览器窗口的名字。它有一个特点，就是浏览器刷新后，该属性保持不变。
所以，可以把值存放在该属性内，然后跨页面、甚至跨域名使用。当然，这个值有可能被其他网站的页面改写。

### 如何获取窗口的尺寸？

`window.innerHeight` - 浏览器窗口的内部高度
`window.innerWidth` - 浏览器窗口的内部宽度
这两个属性返回网页的CSS布局占据的浏览器窗口的高度和宽度，单位为像素。

### 如何调整滚动条位置

首先获取滚动条位置：
`window.scrollX`：滚动条横向偏移
`window.scrollY`：滚动条纵向偏移

调整滚动条位置：
```js
window.scrollTo(0, 300); // 滚动条移动到300px处，两个参数分别是水平、垂直方向偏移
scrollBy(0, 100); // 相对于当前位置，滚动条下移100px
```

### 如何获取浏览器相关信息

通过Window对象的`navigator`属性，其指向一个包含浏览器相关信息的对象。
navigator.userAgent属性返回浏览器的User-Agent字符串，用来标示浏览器的种类

### 如何得到一个元素计算后的属性值

一看这个函数的名字我们就知道问题解决了
```js
var style = window.getComputedStyle("元素", "伪类");
```
第二个参数没有伪类设置为null
例如：
```html
<div id="test" style="height:2px;">123</div>
```
```css
div{
    height:100px !important;
    background:#333;
}
```
获取其计算后的值
```js
var div = document.getElementById('test');
var style=window.getComputedStyle(div,null);
console.log(style.height);//输出结果为100px
```

### 如何模拟点击前进后退按钮的点击？

`window.history` 对象包含浏览器的历史。
`history.back()` - 与在浏览器点击后退按钮相同
`history.forward()` - 与在浏览器中点击按钮向前相同