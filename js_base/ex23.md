## 如何在 HTML 文档中嵌入 JavaScript 脚本？

1.在head标签内或者body标签内使用`<script src="http://xxxx"></script>`嵌入
2.或者通过如下方式嵌入：
```html
<script type="appliation/javascript">
    document.write("<h1>这是一个标题</h1>");
    document.write("<p>这是一个段落。</p>");
</script>
```
3.在 HTML 属性中直接嵌入
例如：
```html
<p onclick="alert('感谢点击');">求点击!</p>
```

## JavaScript 需要预先编译吗？它是如何执行的？

不需要，它是解释性脚本语言，在运行时通过解释器解释执行

## 如何使用 JavaScript 在网页加载完毕时弹窗？
将js脚本放入`</body>`标签之前，如：
```html
<head>
    ...
</head>
<body>
    ...
<script>
    window.onload=function(){
        alert("加载成功！");
    }
</script>
</body>
```

## 代码题
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <script type="application/javascript">
        document.write("<ul><li>码蜂社</li><li>前端</li><li>教程</li></ul>");
    </script>
</body>
</html>
```