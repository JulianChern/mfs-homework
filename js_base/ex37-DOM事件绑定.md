## 问答题
### 什么是事件绑定？我们为什么需要它？
我们可以在事件发生时执行 JavaScript，在事件发生之前，我们需要指定事件的响应函数（也就是告诉计算机，当某件事发生时，需要执行哪些代码）；这个指定过程就是事件绑定。

事件绑定让js对html事件作出响应，实现网页动态化

### 有哪三种方法绑定事件？

1.通过配置HTML元素属性 onXXXX 绑定事件发生时执行的代码，如`<div id=demo onclick="this.innerHTML='谢谢!'">请点击该文本</div>`

2.使用 DOM 来动态的指定事件响应函数，如:
```html
<div id="demo">请点击该文本</div>
<script>
    document.getElementById("demo").onclick = changeText;
    function changeText() {
        document.getElementById("demo").innerHTML = '谢谢！';
    }
</script>
```

3.使用 addEventListener() 指定事件响应函数，如:
```js
document.getElementById("demo").addEventListener("click", function(){
    document.getElementById("demo").innerHTML = '谢谢!'
});
```



### document.getElementById("eleID").onclick = onclickHandle 和 addEventListener() 绑定事件处理函数有何异同？不同之处请至少说出3点。

同：都通过DOM来动态指定元素的响应事件
异：

* `document.getElementById("eleID").onclick = onclickHandle`只能为元素指定一个响应事件，并且后面的会覆盖前面的。而`addEventListener()`可指定多个响应事件
* `addEventListener()`可通过`removeEventListener()`删除绑定的响应事件，而前者不行。
* `addEventListener()`可通过第三个参数`useCapture`来指定事件在捕获或冒泡阶段执行。

### 什么是事件对象？我们如何获取事件对象？
Event 对象代表事件的状态，比如事件在其中发生的元素、键盘按键的状态、鼠标的位置、鼠标按钮的状态。

向绑定事件的响应函数传入event参数即可。

## 代码题
### 请实现标签页效果，样式部分可以自行发挥

### 请实现抽奖效果，实现开始抽奖后，.award不断变化，点击停止后提示用户中奖等级

备选奖项和概率如下

特等奖，1%
一等奖，10%
二等奖，30%
三等奖，40%
鼓励奖：19%
可以参考如下 DOM 结构

```html
<div id="award">点击“开始“按钮，开始抽奖！</div>
<div class="action">
   <button id="start">开 始</button>
   <button id="stop">停 止</button>
</div>
```

代码：<https://github.com/zfhxi/mfs-homework/blob/master/js_base/ex37-EventHandler.html>

预览：<https://zfhxi.github.io/mfs-homework/js_base/ex37-EventHandler.html>