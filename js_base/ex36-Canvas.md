## 问答题

### 什么是 Canvas？Canvas 能干那些事？

Canvas 是一个通过JavaScript 和 HTML的canvas元素来绘制图形的工具。
它可以用于动画、游戏画面、数据可视化、图片编辑以及实时视频处理等方面。

### 画笔颜色，填充颜色各如何设置？

设置画笔的颜色: `strokeStyle = color`
设置图形的填充颜色: `fillStyle = color`

### Canvas 中如何画圆？如何填充圆？

如下：
```js
ctx.arc(x, y, radius, 0, Math.PI*2, true); // 画一个以（x,y）为圆心的以radius为半径的圆，逆时针画。
ctx.fill(); //填充园
```

### Canvas 如何改变坐标系的位置和单位长度？

`translate(x,y)`，移动坐标原点到(x,y)
`scale(x,y)`，分别在x方向和y方向缩放坐标系

## 代码题

### 请使用 Canvas 绘制钟表，并可以动态显示当前时间

js部分代码：<https://github.com/zfhxi/mfs-homework/blob/master/js_base/ex36-clock.js>
html部分代码：<https://github.com/zfhxi/mfs-homework/blob/master/js_base/ex36-index.html>

预览：<https://zfhxi.github.io/mfs-homework/js_base/ex36-index.html>