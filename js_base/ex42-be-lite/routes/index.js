var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

//获取json数据
router.get('/json', (req, res) => {
  var items = [];
  for (i = 0; i < 100; i++) {
    items.push({ id: i + 1, title: "标题" + (i + 1), visitedTimes: 0, commnetNums: 0 })//id，标题，访问次数，评论数
  }
  /*res.json(
    [
      { id: 1, title: "标题1", visitedTimes: 0, commnetNums: 0 },
      { id: 2, title: "标题2", visitedTimes: 0, commnetNums: 0 }
    ]
  );*/
  res.json(JSON.parse(JSON.stringify(items)));
});

router.get('/items', (req, res) => {
  var nums=req.query.itemnums;
  var items = [];
  //生成nums个item对象
  for (i = 0; i < nums; i++) {
    items.push({ id: i + 1, title: "标题" + (i + 1), visitedTimes: 0, commnetNums: 0 })//id，标题，访问次数，评论数
  }
  res.json(JSON.parse(JSON.stringify(items)));
});

module.exports = router;
