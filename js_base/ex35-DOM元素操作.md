## 问答题
### dom 元素常用属性有哪些？

如下：

* nodeName：元素标签名，还有个类似的tagName
* nodeType：元素类型
* className：类名
* id：元素id
* children：子元素列表（HTMLCollection）
* childNodes：子元素列表（NodeList）
* firstChild：第一个子元素
* lastChild：最后一个子元素
* nextSibling：下一个兄弟元素
* previousSibling：上一个兄弟元素
* parentNode、parentElement：父元素

### 如何查找元素？

`getElementById();`，根据id查找
`getElementsByClass();`，根据类名
`getElementsByTagName();`，根据标签名
`getElementsByName();`，根据元素的name属性
`querySelector();`，根据选择器查找第一个
`querySelectorAll();`，根据选择器查找所有
`elementFromPoint();`，根据(x,y)位置查找

### 如何增/删/改/查元素？

**查询**
查询方式参考上一题的查找方法和第一题的元素常用属性。
**增加：**

* createElement()方法，用来生成HTML元素节点。
```
var newDiv = document.createElement("div");
```
createElement方法的参数为元素的标签名，即元素节点的tagName属性。

* createTextNode()方法，用来生成文本节点，参数为所要生成的文本节点的内容。
如:
```js
var newDiv = document.createElement("div");
var newContent = document.createTextNode("Hello");
```
上面代码新建一个div节点和一个文本节点

* createDocumentFragment()方法， 生成一个DocumentFragment对象。
```js
var docFragment = document.createDocumentFragment();
```

**修改**

* `appendChild()`, 在元素的子元素列表末尾添加元素
* `insertBefore()`, 在某个元素之前插入元素
* `replaceChild(new,old)`, 接受两个参数：要插入的元素和要替换的元素

**删除**

使用removeChild()方法

### HTMLCollectioin 和 NodeList 有何异同？

同：

* 都是类数组对象，都有length属性，可以通过for循环迭代
* 都是只读的
* 都是实时的，即文档的更改会立即反映到相关对象上面(有一个例外，document.querySelectorAll返回的NodeList不是实时的)
* 都有item()方法，可以通过item(index)或item("id")获取元素

异：

* HTMLCollection对象具有namedItem()方法，可以传递id或name获得元素
* HTMLCollection的item()方法和通过属性获取元素(document.forms.f1)可以支持id和name，而NodeList对象只支持id



## 代码题
### 请使用 Dom 操作向<ul id="list"></ul> 中插入如下 dom
```html
 <li>1</li>
 <li>2</li>
 ```

代码：
```html
<body>
    <ul id="list"></ul>
    <script type="application/javascript">
        var ulObj = document.getElementById("list");
        var ele;
        for (var i = 1; i < 3; i++) {
            ele = document.createElement("li");
            ele.innerText = i;
            ulObj.appendChild(ele);
        }
    </script>
</body>
```