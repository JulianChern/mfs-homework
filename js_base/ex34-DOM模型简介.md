## 问答题
### 什么是 Dom？为什么要用 Dom？

定义：DOM是Document Object Model，文档对象模型，是HTML和XML文档的编程接口。它提供了对文档的结构化的表述，并定义了一种方式可以使从程序中对该结构进行访问，从而改变文档的结构，样式和内容。
原因：DOM是web页面的完全的面向对象表述，它能够使用 JavaScript脚本语言进行修改，DOM文档结构等价一棵树，一棵树都能被js中的对象表示，文档等价于js对象，因此可通过动态改变js对象来动态改变DOM文档结构以实现动态HTML网页。

### Dom 有哪些常见属性？

doctype， head， body， activeElement， title， characterSet， cookie， innerText， innerHTML， outerHTML

### 如何设置 Cookie？

cookie是一个个键值对（“键=值”的形式）加上分号空格隔开组合而成， 形如： "name1=value1; name2=value2; name3=value3"
通过 JavaScript，可以这样创建 cookie：
```js
document.cookie = "username=Bill Gates";
```
还可以添加有效日期（UTC 时间）。默认情况下，在浏览器关闭时会删除 cookie：
```js
document.cookie = "username=John Doe; expires=Sun, 31 Dec 2017 12:00:00 UTC";
```
通过 path 参数，可以设置 cookie 属于什么路径。默认情况下，cookie 属于当前页。
```js
document.cookie = "username=Bill Gates; expires=Sun, 31 Dec 2017 12:00:00 UTC; path=/";
```

参考自[w3cschool](https://www.w3school.com.cn/js/js_cookies.asp)


### innerText 和 innerHTML 有什么异同？ innerHTML 和 outerHTML 有什么不同？

innerText是一个可写属性，返回元素内包含的文本内容，在多层次的时候会按照元素由浅到深的顺序拼接其内容
innerHTML属性作用和innerText类似，但是不是返回元素的文本内容，而是返回元素的HTML结构，在写入的时候也会自动构建DOM
outerHTML 返回内容还包括本身
```html
<div id="root">
    <p>
        123
        <span>456</span>
    </p>
</div>
```
外层div的innerText返回内容是`123456`
外层div的innerHTML返回内容是:
```html
 <p>
     123
     <span>456</span>
 </p>
```
outerHTML 返回内容是
```html
<div id="root">
    <p>
        123
        <span>456</span>
    </p>
</div>
```

### 我们可以使用 document.write() 动态生成文档流吗？如何可以，如何操作？

可以。
例如：
```js
<body>
   <script type="application/javascript">
        for (var i = 0; i < 3; i++) {
            document.write("<div>This is line " + i + "</div>");
        }
    </script>
</body>
```
显示：
```txt
This is line 0
This is line 1
This is line 2
```



### 在什么时候，document.write() 会重写文档流？

当页面已经渲染完成时，如果再调用write方法，它会先调用open方法，擦除当前文档所有内容，然后再写入

## 代码题

### 请使用 JavaScript 动态生成以下文档
```js
<ul>
 <li>1</li>
 <li>2</li>
 <li>3</li>
</ul>
```

代码如下：
```html
<body>
    <div>
    <script type="application/javascript">
        document.write("<ul>");
        for (var i = 1; i < 4; i++) {
            document.write("<li>" + i + "</li>");
        }
        document.write("</ul>");
    </script>
    </div>
</body>
```