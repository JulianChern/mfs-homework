## 问答题

### 创建对象有几种方式？最常用的是那种？

* 直接创建：
```js
person=new Object(); // 或者使用 {}
person.firstname="Bill";
person.lastname="Gates";
person.age=56;
person.eyecolor="blue";
```

* 字面量
```js
person={firstname:"John",lastname:"Doe",age:50,eyecolor:"blue"};
```

* 使用对象构造器
```js
function Person(firstname,lastname,age,eyecolor){
  this.firstname=firstname;
  this.lastname=lastname;
  this.age=age;
  this.eyecolor=eyecolor;
}
```

最常用的是第三种


### 如何使用对象构造器？对象构造器和函数有什么关系？

当要新创建对象时，可以通过使用new关键字来使用对象构造器来创建新对象。
对象构造器与函数都会使用function做声明，而ES5中没有类的概念，只是使用函数构造器作为“类”的声明。

### 如何遍历所有的对象属性？

使用for...in循环:
```js
var person={fname:"Bill",lname:"Gates",age:56};

for (x in person){
  console.log(x, person[x])
}
```
结果是：
```
fname Bill
lname Gates
age 56
```

## 代码题

### 对于以下对象，请实现输出对象中所有字段名称和其对应的值
```js
var person={fname:"Bill",lname:"Gates",age:56};
```

方法有两种：
```js
var person = {fname:"Bill",lname:"Gates",age:56};
for (x in person){
  console.log(x, person[x])
}
```
OR

```js
var person={fname:"Bill",lname:"Gates",age:56};
var keys=Object.getOwnPropertyNames(person);
for(x in keys){
    console.log(keys[x],person[keys[x]]);
    }
```
