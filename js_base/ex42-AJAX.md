## 问答题

### AJAX 是什么的简称？它和普通的 HTTP 请求有什么不同？

AJAX，Asynchronous JavaScript and XML的简称
不同点：传统HTTP请求是同步的。AJAX实现了浏览器和服务器之间的异步数据传输。

### 传统网页的渲染模式和基于 AJAX 的网页渲染模式有何不同？

传统网页渲染模式在通过HTTP请求传输数据后需要重新渲染整个页面，而AJAX的网页渲染模式，通过AJAX实现异步HTTP请求数据，可无需重新加载整个页面就可以实现局部刷新。

### 如何模拟调试 AJAX 的数据

**1.搭建动态的web服务器**

* npmjs.com中搜索express-generator
* 在本地PC中端执行`sudo npm install -g express-generator`安装express包。
* 在当前目录执行`express be-lite`创建一个简易后端。
* 然后`cd be-lite && npm install`安装相关依赖包。
* 最后在be-lite目录下执行`nmp start`。

**2.构建json请求**

在`be-lite/routes/index.js`中添加：
```js
router.get('/json',function(req,res){
  res.json({data:"hello json!"});
});
```
然后重启服务器，浏览器访问`localhsot:3000/json`即可收到模拟的数据。

### 如何兼容老浏览器创建 XMLHttpRequest 对象？

```js
function createXHR(){
    var xhr = null;
    try {
        // Firefox, Opera 8.0+, Safari，IE7+
        xhr = new XMLHttpRequest();
    }
    catch (e) {
        // Internet Explorer
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                xhr = null;
            }
        }
    }
    return xhr;
}
```

### XMLHttpRequest 对象有哪几个常用方法？分别对应的含义如何？

```js
open(method, url, async,);
```
初始化一个请求准备发送

* method 参数是用于请求的 HTTP 方法。值包括 GET、POST 和 HEAD。
* url 参数是请求的主体。大多数浏览器实施了一个同源安全策略，并且要求这个 URL 与包含脚本的文本具有相同的主机名和端口。
* async 参数指示请求使用应该异步地执行。如果这个参数是 false，请求是同步的，后续对 send() 的调用将阻塞，直到响应完全接收。如果这个参数是 true 或省略，请求是异步的，且通常需要一个 onreadystatechange 事件句柄。

```js
send(body);
```
发送HTTP请求

* 如果通过调用 open() 指定的 HTTP 方法是 POST 或 PUT，body 参数指定了请求体，作为一个字符串或者 Document 对象。如果请求体不是必须的话，这个参数就为 null。对于任何其他方法，这个参数是不可用的，应该为 null。


### 常见的 HTTP 请求头有哪些？如何使用 AJAX 设置 HTTP 请求头？

常见HTTP请求头：

* Accept：浏览器能够处理的内容类型
* Accept-Charset：浏览器能够处理的字符集
* Accept-Encoding：浏览器能够处理的压缩编码
* Accept-Language：浏览器当前设置的语言
* Connection：浏览器与服务器的连接类型
* Cookie：当前页面的cookie
* Referer:发送请求的页面的URI

设置方法：
```js
setRequestHeader(name, value)
```
指定了一个 HTTP 请求的头部，它应该包含在通过后续 send() 调用而发布的请求中。这个方法只有当 readyState 为 1 的时候才能调用，例如，在调用了 open() 之后，但在调用 send() 之前。

* name 参数是要设置的头部的名称。这个参数不应该包括空白、冒号或换行。
* value 参数是头部的值。这个参数不应该包括换行。


### 如何使用 AJAX 发起 POST 请求？

初始化POST请求：`xhr.open('post', '/search', true);`
绑定`onreadstatechange`事件，以实现一旦接受到返回值并处理:
```js
xhr.onreadystatechange = function () {
    if (xhr.readyState == 4 && xhr.status == 200) {
        alert(xhr.responseText);
    }
};
```
设置请求头，`xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");`
设置传递的参数，`xhr.send('term=mark&in=titlesposts');`


## 代码题
请使用 AJAX 在自己的网页获取数据，并将数据以你喜欢的方法展现出来（可以渲染 dom，可以打印 console）

使用express的路由响应客户端请求：
```js
router.get('/items', (req, res) => {
  var nums=req.query.itemnums;//获取get请求URL后的参数
  var items = [];
  //生成nums个item对象
  for (i = 0; i < nums; i++) {
    items.push({ id: i + 1, title: "标题" + (i + 1), visitedTimes: 0, commnetNums: 0 })//id，标题，访问次数，评论数
  }
  res.json(JSON.parse(JSON.stringify(items)));
});
```

通过ajax发送请求：
```js
function getItems(callback) {
    var xhr = new XMLHttpRequest();
    xhr.open("get", "/items?itemnums=10", true);
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 && xhr.status == 200) {
            callback(JSON.parse(xhr.responseText));
        }
    };
    xhr.send(null);
}
```

最终调用：
```js
window.onload = () => { getItems(renderDomByData); }
```

完整代码见：<https://github.com/zfhxi/mfs-homework/tree/master/js_base/ex42-be-lite>

效果：

![](https://work.mafengshe.com/static/upload/article/pic1576206003488.jpg)