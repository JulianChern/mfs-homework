var items = [];//存放所有的item样本

//常量声明
var itemsnumPerPage = 12;//每页item数
var testItemNumTotal=101;//测试样本总数
//变量声明
var itemsnumTotal = 0;//item样本实际总数
var pagenumTotal = 0;//页总数

//获取dom对象
var item_list = document.getElementById("item-list");
var nav_page = document.getElementById("nav-page");

//生成所有item样本
function generateItems() {
    for (i = 0; i < testItemNumTotal; i++) {
        items.push({ id: i + 1, title: "标题" + (i + 1), visitedTimes: 0, commnetNums: 0 })//id，标题，访问次数，评论数
    }
    itemsnumTotal=items.length;
    pagenumTotal = Math.ceil(itemsnumTotal / itemsnumPerPage);
}
//获得某页的item样本，长度为itemsnumPerPage
function getItemsByPageNo(pageNo) {
    return items.slice(pageNo * itemsnumPerPage, (pageNo + 1) * itemsnumPerPage);
}

//生成页导航栏
function genreatePageNav(pageTotal) {
    newSubEle = document.createElement("a");
    newSubEle.innerText = "<";
    newSubEle.setAttribute("href", "#");
    newSubEle.setAttribute("page_index", -1)
    newSubEle.setAttribute("class", "other-page")
    newSubEle.addEventListener("click", jumpToPage);
    nav_page.appendChild(newSubEle);
    for (i = 0; i < pageTotal; i++) {
        newSubEle = document.createElement("a");
        newSubEle.innerText = i + 1;
        newSubEle.setAttribute("href", "#");
        newSubEle.setAttribute("page_index", i)
        newSubEle.setAttribute("class", "other-page")
        newSubEle.addEventListener("click", jumpToPage);
        nav_page.appendChild(newSubEle);
    }
    newSubEle = document.createElement("a");
    newSubEle.innerText = ">";
    newSubEle.setAttribute("href", "#");
    newSubEle.setAttribute("page_index", pageTotal)
    newSubEle.setAttribute("class", "other-page")
    newSubEle.addEventListener("click", jumpToPage);
    nav_page.appendChild(newSubEle);
}

//生成一个item样本
function generateItemSample(item) {
    itemSample = document.createElement("div");
    itemSample.setAttribute("class", "item");
    itemSample.innerHTML =
        '<div class="item-wrapper">'
        + '<div class="item-id">' + item.id + '</div>'
        + '<div class="item-icon iconfont icon-Smile"></div>'
        + '<div class="item-title">' + item.title + '</div>'
        + '<div class="item-info">'
        + '<span class="item-visited iconfont icon-changyongicon-">' + item.visitedTimes + '</span>'
        + '<span class="item-comment iconfont icon-liuyan">' + item.commnetNums + '</span>'
        + '</div>'
        + '<div class="item-smiling"></div>'
        + '</div>';
    return itemSample;
}

//将某页的item样本推到item-list元素中
function pushItemListByPageNum(pageNo) {
    while (item_list.hasChildNodes())
        item_list.removeChild(item_list.lastChild);
    itemsCurPage = getItemsByPageNo(pageNo);
    itemDomsCurPage = itemsCurPage.map((item) => { return generateItemSample(item); });
    itemDomsCurPage.forEach((dom) => { item_list.appendChild(dom); });
}

//跳转到哪个页面
function jumpToPage(Event) {
    curPage = document.getElementsByClassName("cur-page")[0];
    if (!curPage)//未找到，则默认第一页
        curPage = document.getElementsByClassName("other-page")[1];
    targetPage = Event.target;
    if (!targetPage)//未找到，则默认第一页
        targetPage = document.getElementsByClassName("other-page")[1];
    curPageIndex = parseInt(curPage.getAttribute("page_index"));
    targetPageIndex = parseInt(targetPage.getAttribute("page_index"));
    if (-1 == targetPageIndex) {//如果点击的是上一页
        if (0 == curPageIndex) return;//如果当前页是第一页
        targetPage = curPage.previousSibling;
        targetPageIndex = curPageIndex - 1;
    }
    else if (pagenumTotal == targetPageIndex) {//如果点击的是下一页
        if (pagenumTotal - 1 == curPageIndex) return;//如果当前页是最后一页
        targetPage = curPage.nextSibling;
        targetPageIndex = curPageIndex + 1;
    }
    curPage.setAttribute("class", "other-page");
    targetPage.setAttribute("class", "cur-page");
    pushItemListByPageNum(targetPageIndex);
}

//生成item样本
generateItems();
//生成页面导航
genreatePageNav(pagenumTotal);
//模拟点击第一页
window.onload = () => { document.getElementsByClassName("other-page")[1].click(); }