//获取画布上小文
var clock = document.getElementById('clock');
var ctx = clock.getContext('2d');
//获取画布大小
var WIDTH = clock.width;
var HEIGHT = clock.height;
var RADIUS = WIDTH / 2;
var twopi = Math.PI * 2;
//平移画布，坐标原点到画布中心
ctx.translate(RADIUS, RADIUS);
var hours = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2]
var weekDay = ['日', '一', '二', '三', '四', '五', '六'];

//切换时钟时针刻度显示为罗马数字/阿拉伯数字
var sbt = document.getElementById('HScale');
sbt.onclick = function () {
    if (3 == hours[0]) {
        hours = ['Ⅲ', 'Ⅳ', 'Ⅴ', 'Ⅵ', 'Ⅶ', 'Ⅷ', 'Ⅸ', 'Ⅹ', 'Ⅺ', 'Ⅻ', 'Ⅰ', 'Ⅱ']
        sbt.innerHTML = "常规时钟";
    }
    else {
        hours = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2]
        sbt.innerHTML = "罗马时钟";
    }
};

//画刻度
function drawScale() {
    ctx.save();
    //画时钟的边框
    ctx.beginPath();
    ctx.lineWidth = 10;//边框宽度
    ctx.arc(0, 0, RADIUS - 5, 0, twopi, false);
    ctx.stroke();
    //画时针的刻度
    //设置刻度的字体
    ctx.font = "25px serif";
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    hours.forEach(function (number, i) {
        var radian = twopi / 12 * i;
        x = Math.cos(radian) * (RADIUS - 30);
        y = Math.sin(radian) * (RADIUS - 30);
        ctx.fillText(number, x, y);
    });
    //秒针的刻度
    for (var i = 0; i < 60; i++) {
        var radian = twopi / 60 * i;
        x = Math.cos(radian) * (RADIUS - 15);
        y = Math.sin(radian) * (RADIUS - 15);
        ctx.beginPath();
        ctx.fillStyle = (0 == i % 5) ? 'black' : '#ccc';
        ctx.arc(x, y, 3, 0, twopi, false);
        ctx.fill();
    }
    ctx.restore();
}
//画时钟上的中心黑点
function drawDot() {
    ctx.beginPath();
    ctx.fillStyle = 'black';
    ctx.arc(0, 0, 10, 0, twopi, false);
    ctx.fill();
}
function drawDotforSecond() {
    ctx.beginPath();
    ctx.fillStyle = '#c14443';
    ctx.arc(0, 0, 6, 0, twopi, false);
    ctx.fill();
}

//画时针
function drawHour(hour, minute) {
    ctx.save();
    ctx.beginPath();
    var h_radian = twopi / 12 * hour;
    var m_radian = twopi / 12 / 60 * minute;
    ctx.rotate(h_radian + m_radian);
    ctx.lineWidth = 8;
    ctx.lineCap = 'round';
    ctx.moveTo(0, 12);
    ctx.lineTo(0, -RADIUS / 2);
    ctx.stroke();
    ctx.restore();
}
//画分针
function drawMinute(minute) {
    ctx.save();
    ctx.beginPath();
    var radian = twopi / 60 * minute;
    ctx.rotate(radian);
    ctx.lineWidth = 4;
    ctx.lineCap = 'round';
    ctx.moveTo(0, 14);
    ctx.lineTo(0, -RADIUS + 60);
    ctx.stroke();
    ctx.restore();
}
//画秒针
function drawSecond(second) {
    ctx.save();
    ctx.beginPath();
    var radian = twopi / 60 * second;
    ctx.rotate(radian);
    ctx.lineWidth = 2;
    ctx.lineCap = 'round';
    ctx.strokeStyle = '#c14443';
    ctx.moveTo(0, 16);
    ctx.lineTo(0, -RADIUS + 20);
    ctx.stroke();
    ctx.restore();
}

//直接显示数字时间
function showDigitalTime(hour, minute, second) {
    ctx.fillText(hour + ':' + minute + ':' + second, 0, 30);
}

//绘制所有
function drawAll() {
    ctx.clearRect(-RADIUS, -RADIUS, WIDTH, HEIGHT);//清空画布
    var now = new Date();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    drawDot();
    drawScale();
    drawHour(hour, minute);
    drawMinute(minute);
    drawSecond(second);
    drawDotforSecond();
    showDigitalTime(now, hour, minute, second);
}

function showDigitalTime(now, hour, minute, second) {
    document.getElementById('currentTime').innerHTML = now.getFullYear() + "-"
        + fillupWidth(now.getMonth() + 1) + '-'
        + fillupWidth(now.getDate()) + ' '
        + fillupWidth(hour) + ':'
        + fillupWidth(minute) + ':'
        + fillupWidth(second) + ' '
        + '星期' + weekDay[now.getDay()];
}
function fillupWidth(number) { return (number > 9 ? '' : '0') + number }

// window.onload = drawAll();
//设置定时器
setInterval(drawAll, 100);