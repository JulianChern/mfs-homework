var items = [];//存放所有的item样本
var shoppingCart = [];//购物车中的订单

//常量声明
var itemsnumPerPage = 12;//每页item数
var testItemNumTotal = 101;//测试样本总数
//变量声明
var itemsnumTotal = 0;//item样本实际总数
var pagenumTotal = 0;//页总数

//获取dom对象
var item_list = document.getElementById("item-list");
var nav_page = document.getElementById("nav-page");
var orderItem_list = document.getElementById("order-item-list");

//生成所有item样本
function generateItems() {
    for (var i = 0; i < testItemNumTotal; i++) {
        items.push({ id: i + 1, title: "商品" + (i + 1), visitedTimes: 0, commentTimes: 0 })//id，标题，访问次数，评论数
    }
    itemsnumTotal = items.length;
    pagenumTotal = Math.ceil(itemsnumTotal / itemsnumPerPage);
}
//获得某页的item样本，长度为itemsnumPerPage
function getItemsByPageNo(pageNo) {
    return items.slice(pageNo * itemsnumPerPage, (pageNo + 1) * itemsnumPerPage);
}

//生成页导航栏
function genreatePageNav(pageTotal) {
    var newSubEle = document.createElement("a");
    newSubEle.innerText = "<";
    newSubEle.setAttribute("href", "#");
    newSubEle.setAttribute("page_index", -1)
    newSubEle.setAttribute("class", "other-page")
    newSubEle.addEventListener("click", jumpToPage);
    nav_page.appendChild(newSubEle);
    for (i = 0; i < pageTotal; i++) {
        var newSubEle = document.createElement("a");
        newSubEle.innerText = i + 1;
        newSubEle.setAttribute("href", "#");
        newSubEle.setAttribute("page_index", i)
        newSubEle.setAttribute("class", "other-page")
        newSubEle.addEventListener("click", jumpToPage);
        nav_page.appendChild(newSubEle);
    }
    var newSubEle = document.createElement("a");
    newSubEle.innerText = ">";
    newSubEle.setAttribute("href", "#");
    newSubEle.setAttribute("page_index", pageTotal)
    newSubEle.setAttribute("class", "other-page")
    newSubEle.addEventListener("click", jumpToPage);
    nav_page.appendChild(newSubEle);
}

//生成一个item样本
function generateItemSample(item) {
    var itemSample = document.createElement("div");
    itemSample.setAttribute("class", "item");
    itemSample.innerHTML =
        '<div class="item-wrapper">'
        + '<div class="item-id">' + item.id + '</div>'
        + '<div class="item-icon iconfont icon-Smile"></div>'
        + '<div class="item-title">'
        + '<span>' + item.title + '</span>'
        + '<button id="addShop">+</button>'
        + '</div>'
        + '<div class="item-info">'
        + '<span class="item-visited iconfont icon-changyongicon-">' + item.visitedTimes + '</span>'
        + '<span class="item-comment iconfont icon-liuyan">' + item.commentTimes + '</span>'
        + '</div>'
        + '<div class="item-smiling"></div>'
        + '</div>';
    return itemSample;
}

//将某页的item样本推到item-list元素中
function pushItemListByPageNum(pageNo) {
    while (item_list.hasChildNodes())
        item_list.removeChild(item_list.lastChild);
    var itemsCurPage = getItemsByPageNo(pageNo);
    var itemDomsCurPage = itemsCurPage.map((item) => { return generateItemSample(item); });
    itemDomsCurPage.forEach((dom) => { item_list.appendChild(dom); });
}

//跳转到哪个页面
function jumpToPage(Event) {
    var curPage = document.getElementsByClassName("cur-page")[0];
    if (!curPage)//未找到，则默认第一页
        curPage = document.getElementsByClassName("other-page")[1];
    var targetPage = Event.target;
    if (!targetPage)//未找到，则默认第一页
        targetPage = document.getElementsByClassName("other-page")[1];
    var curPageIndex = parseInt(curPage.getAttribute("page_index"));
    var targetPageIndex = parseInt(targetPage.getAttribute("page_index"));
    if (-1 == targetPageIndex) {//如果点击的是上一页
        if (0 == curPageIndex) return;//如果当前页是第一页
        targetPage = curPage.previousSibling;
        targetPageIndex = curPageIndex - 1;
    }
    else if (pagenumTotal == targetPageIndex) {//如果点击的是下一页
        if (pagenumTotal - 1 == curPageIndex) return;//如果当前页是最后一页
        targetPage = curPage.nextSibling;
        targetPageIndex = curPageIndex + 1;
    }
    curPage.setAttribute("class", "other-page");
    targetPage.setAttribute("class", "cur-page");
    pushItemListByPageNum(targetPageIndex);
    bindBtnResponeIfAddOrder();
}

//插入到购物车订单数组中
function insertInShoppingCart(orderObj) {
    var insertFlag = false;
    shoppingCart.forEach((ele) => {
        if (ele.id == orderObj.id) {
            ele.orderNum++;
            insertFlag = true;
        }
    });
    if (false == insertFlag) {
        shoppingCart.push({ id: orderObj.id, name: orderObj.name, orderNum: orderObj.orderNum });
    }
}

//增加一个订单响应事件
function addOrderNumByOne() {
    var selectedGood = event.target.parentNode;
    var orderId = Number(selectedGood.getAttribute("order_id"));
    shoppingCart.forEach((ele) => {
        if (ele.id == orderId)
            ele.orderNum++;
    });
    refreshShopCartDom();
};
//减少一个订单响应事件
function subOrderNumByOne() {
    var selectedGood = event.target.parentNode;
    var orderId = Number(selectedGood.getAttribute("order_id"));
    shoppingCart.forEach((ele) => {
        if (ele.id == orderId)
            ele.orderNum--;
    });
    refreshShopCartDom();
};

//重绘购物车
function refreshShopCartDom() {
    while (orderItem_list.hasChildNodes())
        orderItem_list.removeChild(orderItem_list.lastChild);
    if (0 == shoppingCart.length) {
        storageObj("shopcart", shoppingCart);
        return;
    }
    shoppingCart.forEach((item) => {
        if (0 !== item.orderNum) {//订单数不为0
            var orderItemDom = document.createElement("div");
            orderItemDom.setAttribute("order_id", item.id.toString());
            orderItemDom.innerHTML = '<span>' + item.name + '</span>'
                + " "
                + '<button id="subOrderNum" onclick="subOrderNumByOne()">-</button>'
                + '<span>' + item.orderNum + '</span>'
                + '<button id="addOrderNum" onclick="addOrderNumByOne()">+</button>';
            orderItem_list.appendChild(orderItemDom);
        }
    });
    var clearAllBtn = document.createElement("button");
    clearAllBtn.addEventListener("click", () => {
        shoppingCart.length = 0;
        refreshShopCartDom();
    });
    if(false==orderItem_list.hasChildNodes()){
        return;
    }
    clearAllBtn.innerHTML = "清空";
    clearAllBtn.setAttribute("class", "clearAll-btn");
    orderItem_list.appendChild(clearAllBtn);
    storageObj("shopcart", shoppingCart);
}

//使用localStorage存储数组对象
function storageObj(keyName, arrayObj) {
    var arrayStr = JSON.stringify(arrayObj);
    localStorage.setItem(keyName, arrayStr);
}

//从localStorage中取数组对象
function getObjFromStorage(keyName) {
    var arrayObj = JSON.parse(localStorage.getItem(keyName));
    if (!arrayObj)
        arrayObj = [];
    return arrayObj;
}

//绑定订单添加按钮响应事件
function bindBtnResponeIfAddOrder() {
    var addShopBtns = document.querySelectorAll("button#addShop");
    addShopBtns.forEach(btn => {
        btn.addEventListener("click", (Event) => {
            var selectedGood = Event.target.parentNode.parentNode;
            var goodId = Number(selectedGood.children[0].innerText);
            var goodName = String(selectedGood.children[2].firstChild.innerText);
            var goodOrderNum = 1;
            insertInShoppingCart({ id: goodId, name: goodName, orderNum: goodOrderNum });
            refreshShopCartDom();
        });
    });

}

window.onload = () => {
    //生成商品item样本
    generateItems();
    if (1 < pagenumTotal) {
        genreatePageNav(pagenumTotal);//生成页面导航
        document.getElementsByClassName("other-page")[1].click();//模拟点击第一页
    }
    else
        pushItemListByPageNum(0);
    //读取localStroage
    shoppingCart = getObjFromStorage("shopcart");
    //绑定订单添加按钮响应事件
    bindBtnResponeIfAddOrder();
    //刷新购物车
    refreshShopCartDom();
};