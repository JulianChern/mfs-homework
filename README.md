# czm的作业库

> 码蜂社 前端基础班 作业答题库

**码蜂社和czm版权所有，转载请注明出处**

当前目录结构：
```
.
├── css_base
│   ├── ex11-css语法.html
│   ├── ex11-css语法.md
│   ├── ex11-demo.css
│   ├── ex12-dropdown.html
│   ├── ex12-prove_priori.html
│   ├── ex12-选择器.html
│   ├── ex12-选择器.md
│   ├── ex13-bgimg.jpeg
│   ├── ex13-button_sprite.html
│   ├── ex13-button_sprite.png
│   ├── ex13-常用属性1.html
│   ├── ex13-常用属性1.md
│   ├── ex14-iconfont
│   ├── ex14-常用属性2-border.html
│   ├── ex14-常用属性2_homework.html
│   ├── ex14-常用属性2_iconfont.html
│   ├── ex14-常用属性.md
│   ├── ex15-盒模型2.html
│   ├── ex15-盒模型3.html
│   ├── ex15-盒模型.html
│   ├── ex15-盒模型.md
│   ├── ex16-浮动.html
│   ├── ex16-浮动.md
│   ├── ex16-浮动导航.html
│   ├── ex17-flex.html
│   ├── ex17-flex.md
│   ├── ex17-flex列表.html
│   ├── ex17-flex圣杯布局.html
│   ├── ex17-flex完全居中.html
│   ├── ex17-骰子的布局.html
│   ├── ex18-实战1
│   ├── ex19.md
│   ├── ex19-taiji.html
│   ├── ex19-定位和z-index.html
│   ├── ex19-浮动导航固定.html
│   ├── ex19-绘图事例.html
│   ├── ex20-col1_12.html
│   ├── ex20.md
│   ├── ex20-nav.css
│   ├── ex20-nav.html
│   ├── ex20-nav.js
│   ├── ex20-响应式.html
│   ├── ex20-媒体查询.html
│   ├── ex21-flex布局.html
│   ├── ex21.md
│   ├── ex21-单列布局.html
│   ├── ex21-双飞翼布局.html
│   ├── ex21-圣杯布局_flex方案.html
│   ├── ex21-圣杯布局_float方案.html
│   ├── ex21-多列布局.html
│   ├── ex21-居中demo.html
│   ├── ex21-居中.html
│   ├── ex22-bg.png
│   ├── ex22.md
│   ├── ex22-动态旋转加载条.html
│   ├── ex22-动画.html
│   ├── ex22-条形加载动画.html
│   ├── ex22-渐变.html
│   ├── ex22-渐变效果导航栏.html
│   ├── ex22-环形加载动画.html
│   └── ex22-转换.html
├── html_base
│   ├── ex01.md
│   ├── ex02-HTML结构.html
│   ├── ex02-HTML结构.md
│   ├── ex03-HTML 语法和常见标签及其属性.html
│   ├── ex03-HTML 语法和常见标签及其属性.md
│   ├── ex03-img_test.jpeg
│   ├── ex04-表单一_homework.html
│   ├── ex04-表单一.html
│   ├── ex04-表单一.md
│   ├── ex05-表单二_homework.html
│   ├── ex05-表单二.html
│   ├── ex05-表单二.md
│   ├── ex06-frameset.html
│   ├── ex06-iframe_homework.html
│   ├── ex06-iframe.html
│   ├── ex06-iframe.md
│   ├── ex07-test.css
│   ├── ex07-test.js
│   ├── ex07-外部资源引入和路径表示.html
│   ├── ex07-外部资源引入和路径表示.md
│   ├── ex08-语义化标签.html
│   ├── ex08-语义化标签.md
│   ├── ex09-URL.md
│   ├── ex10-html5新特性.html
│   ├── ex10-html5新特性和常见问题总结.md
│   └── ex10-test_encode.html
├── js_base
│   ├── ex23-demo0.html
│   ├── ex23-demo2.html
│   ├── ex23.md
│   ├── ex24-基础概念.md
│   ├── ex25-数据类型.md
│   ├── ex26-字符串.md
│   ├── ex27-数组.md
│   ├── ex28-函数.md
│   ├── ex29-对象.md
│   ├── ex30.html
│   ├── ex30-运算符.md
│   ├── ex31.html
│   ├── ex31-控制流.md
│   ├── ex32.html
│   ├── ex32-错误处理.md
│   ├── ex33-严格格式.md
│   ├── ex34-DOM模型简介.md
│   ├── ex34.html
│   ├── ex35-DOM元素操作.md
│   ├── ex35.html
│   ├── ex36-Canvas.md
│   ├── ex36-clock.js
│   ├── ex36-clock-style.css
│   ├── ex36-index.html
│   ├── ex37-DOM事件绑定.md
│   ├── ex37-EventHandler.html
│   ├── ex37-绑定事件3methods.html
│   ├── ex38-DOM实战_翻页.html
│   ├── ex38-DOM实战_表单验证.html
│   ├── ex38-iconfont
│   ├── ex38-page_turn.css
│   ├── ex38-page_turn.js
│   ├── ex39-BOM基础.html
│   ├── ex39-BOM基础.md
│   ├── ex40-shoppingcart.css
│   ├── ex40-shoppingcart.js
│   ├── ex40-浏览器存储.html
│   ├── ex40-浏览器存储.md
│   ├── ex40-购物车.html
│   ├── ex41-jsonp_demo.html
│   ├── ex41-跨域.md
│   ├── ex41-跨域访问测试1.html
│   ├── ex41-跨域访问测试2.html
│   ├── ex41-降域访问测试1.html
│   ├── ex41-降域访问测试2.html
│   ├── ex42-AJAX.md
│   ├── ex42-be-lite
│   ├── ex42-XHR_demo.html
│   ├── ex43-code.html
│   ├── ex43-标准对象.md
│   ├── ex44-add_removeClass.html
│   ├── ex44-getInfoFromHtml.html
│   ├── ex44-source.html
│   ├── ex44-密码正则.html
│   └── ex44-正则表达式.md
└── README.md
```
