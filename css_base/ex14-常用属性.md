## font 属性可以缩写哪些子属性？它们的参数各是什么，有何含义？

可缩写的属性以及含义：
* font-size：字体大小，如16px
* font-family：字体，宋体、微软雅黑、Arial等
* font-weight：文字粗度，常用的就是默认值regular和粗体bold
* line-height：行高，可以用百分比、倍数或者固定尺寸。如1.2em

## 如何实现单行文本的垂直居中？

使line-height与height属性的值等大小。

## 如何实现文本的水平居中？

text-align属性值设为center

## 什么是 iconfont？使用它有什么优势？如何使用 iconfont？

iconfont是阿里巴巴提供的图标库，
以下是img实现图标的缺点，而iconfont克服了这些缺点就成为其优势：

* 不方便调节大小
* 耗费流量
* 不同分辨率下需要不同图片
* 视网膜下不清晰

使用方法，登录进入iconfont.cn，将要使用的图标加入购物车后，下载代码到本地，解压后点击demo_index.html，就能看到官方的说明。

## 文本相关的属性哪些是可以继承的？

font-family, font-size, font-style,
font-variant, font-weight, font, letter-spacing,
line-height, text-align, text-indent,
texttransform, word-spacing
都可继承

## 什么是 CSS 边框？如何使用边框？

元素外边距内就是元素的的边框 (border)。元素的边框就是围绕元素内容和内边据的一条或多条线。
每个边框有 3 个方面：宽度、样式，以及颜色。

使用:

css中分别指定：

* border-width：边框宽度
* border-color：边框颜色
* border-style：边框样式（solid、dashed）

支持合写
```css
border: solid 1px #333;
```

## 请使用 border 实现四个方向的直角三角形, 请使用 border 属性画出一个正三角形，方向不限, 请使用 border 属性画出一个直角梯形，方向不限

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ex14-常用属性2</title>
    <style>
        .top-triangle {
            height: 0px;
            width: 0px;
            border: 100px solid transparent;
            border-top-color: seagreen;
        }

        .bottom-triangle {
            height: 0px;
            width: 0px;
            border: 100px solid transparent;
            border-bottom-color: gold;
        }

        .left-triangle {
            height: 0px;
            width: 0px;
            border: 100px solid transparent;
            border-left-color: darkgoldenrod;
        }

        .right-triangle {
            height: 0px;
            width: 0px;
            border: 100px solid transparent;
            border-right-color: sandybrown;
        }
        .regular-triangle{
            height: 0;
            width: 0;
            border: 100px solid transparent;
            border-left: calc(100px*1.73) solid fuchsia;
        }
        .right-trapezoid{
            height: 100px;
            width: 100px;
            border: 100px solid transparent;
            border-left: 0;
            border-top-color: hotpink;
        }
    </style>
</head>

<body>
    <div class="top-triangle"></div>
    <div class="bottom-triangle"></div>
    <div class="left-triangle"></div>
    <div class="right-triangle"></div>
    <div class="regular-triangle"></div>
    <div class="right-trapezoid"></div>
</body>

</html>
```

效果：

![](https://work.mafengshe.com/static/upload/article/pic1568090166638.jpg)