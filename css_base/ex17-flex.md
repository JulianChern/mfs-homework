## 当父元素设置为 display: flex 时，子元素的摆放方式更像 display: block 还是 display: inline？请解释为什么。

更像display:inline，因为flex用来为盒状模型提供最大的灵活性。block没有inline灵活。

## flexbox 有哪两个轴？他们的方向可以改变吗？如果可以改变，通过设置什么属性可以改变？

有主轴和交叉轴

可改变：
flex-direction决定主轴的方向
align-items定义项目在交叉轴上如何对齐

## 有哪些属性可以设置在 container 上的？他们分别有哪些属性？分别代表什么意思

如下属性可设置在容器上：
```css
flex-direction
flex-wrap
flex-flow
justify-content
align-items
align-content
```
### flex-direction

决定主轴的方向（即项目的排列方向）。

```text
row（默认值）：主轴为水平方向，起点在左端。
row-reverse：主轴为水平方向，起点在右端。
column：主轴为垂直方向，起点在上沿。
column-reverse：主轴为垂直方向，起点在下沿。
```

### flex-wrap

默认情况下，项目都排在一条线（又称"轴线"）上。flex-wrap属性定义，如果一条轴线排不下，如何换行。

```text
nowrap（默认）：不换行。
wrap：换行，第一行在上方。
wrap-reverse：换行，第一行在下方。
```

###  flex-flow

flex-flow属性是flex-direction属性和flex-wrap属性的简写形式，默认值为row nowrap

### justify-content属性

justify-content属性定义了项目在主轴上的对齐方式。

```text
flex-start（默认值）：左对齐
flex-end：右对齐
center： 居中
space-between：两端对齐，项目之间的间隔都相等。
space-around：每个项目两侧的间隔相等。所以，项目之间的间隔比项目与边框的间隔大一倍。
```

### align-items

align-items属性定义项目在交叉轴上如何对齐

```text
flex-start：交叉轴的起点对齐。
flex-end：交叉轴的终点对齐。
center：交叉轴的中点对齐。
baseline: 项目的第一行文字的基线对齐。
stretch（默认值）：如果项目未设置高度或设为auto，将占满整个容器的高度。
```

### align-content

align-content属性定义了多根轴线的对齐方式。如果项目只有一根轴线，该属性不起作用。

```text
flex-start：与交叉轴的起点对齐。
flex-end：与交叉轴的终点对齐。
center：与交叉轴的中点对齐。
space-between：与交叉轴两端对齐，轴线之间的间隔平均分布。
space-around：每根轴线两侧的间隔都相等。所以，轴线之间的间隔比轴线与边框的间隔大一倍。
stretch（默认值）：轴线占满整个交叉轴。
```

## 有哪些属性可以设置在 item 上的？他们分别有哪些属性？分别代表什么意思

如下属性可设置在item上：

```css
order
flex-grow
flex-shrink
flex-basis
flex
align-self
```

### order属性

order属性定义项目的排列顺序。数值越小，排列越靠前，默认为0。

### flex-grow

flex-grow属性定义项目的放大比例，默认为0，即如果存在剩余空间，也不放大。

### flex-shrink

flex-shrink属性定义了项目的缩小比例，默认为1，即如果空间不足，该项目将缩小。

### flex-basis

flex-basis属性定义了在分配多余空间之前，项目占据的主轴空间（main size）。浏览器根据这个属性，计算主轴是否有多余空间。它的默认值为auto，即项目的本来大小。

### flex

flex属性是flex-grow, flex-shrink 和 flex-basis的简写，默认值为0 1 auto。后两个属性可选。
该属性有两个快捷值：auto (1 1 auto) 和 none (0 0 auto)。

### align-self

align-self属性允许单个项目有与其他项目不一样的对齐方式，可覆盖align-items属性。默认值为auto，表示继承父元素的align-items属性，如果没有父元素，则等同于stretch。

## 如何使用 flex 实现完全居中

html代码：
```html
<div class="container">
    <div class="item"></div>
</div>
```

css代码：
```css
.container{
    width: 400px;
    height: 500px;
    border: 5px solid #f00;
    background-color: yellow;
    display: flex;
    justify-content: center;
    align-items: center;
}
.item{
    height: 100px;
    width: 100px;
    background-color: black;
    border:2px solid white;
}
```

## 如何使用 flex 实现 列表（从左往右排布，可以多行，最后一行左对齐）

html代码：
```html
<div class="container">
    <div class="item">1</div>
    <div class="item">2</div>
    <div class="item">3</div>
    <div class="item">4</div>
    <div class="item">5</div>
    <div class="item">6</div>
    <div class="item">7</div>
</div>
```
css代码：
```css
.container {
    height: 350px;
    width: 550px;
    border: 5px solid black;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    align-content: flex-start;
}
.container:after {
    content: "";
    width: 100px;
    margin: 10px;
}
.item {
    margin: 10px;
    height: 150px;
    width: 100px;
    background-color: red;
    text-align: center;
    line-height: 150px;
}
.item:nth-child(3n+1) {
    background-color: yellow;
}
.item:nth-child(3n+2) {
    background-color: hotpink;
}
```

![](https://work.mafengshe.com/static/upload/article/pic1568816351155.jpg)


其他解决方案见<https://segmentfault.com/q/1010000008688280>、<https://blog.csdn.net/qq_30376375/article/details/93142191>



## 请使用 flex 实现圣杯布局，注意：圣杯布局要求优先加载中间部分的内容

html代码：
```html
<header>头部</header>
<div class="main">
    <div id="content">主内容自适应宽度</div>
    <div id="left">侧边栏1固定宽度</div>
    <div id="right">侧边栏2固定宽度</div>
</div>
<footer>底部</footer>
```

css代码：
```css
*{
    text-align: center;
    line-height: 50px;
    margin:0;
    padding: 0;
}
body{
    /*header,main,footer shown by column*/
    display: flex;
    flex-direction: column;
}
header,footer{
    flex:0 0 50px;/*header,footer not grow&shrink, always 50px*/
    background-color: #aaa;
}
.main{
    display: flex;
    flex:1;/*get the rest of space*/
}
#content{
    background-color: skyblue;
    flex:1;/*get the whole rest space by column direction*/
}
#left,#right{
    background-color: powderblue;
    flex:0 0 200px;/*fixed as 100px*/
}
#left{
    order:-1;/*make #left first*/
}
```



## 请完成 骰子的布局

html代码：

```html
<div class="one-face">
    <span class="dot"></span>
</div>
<div class="two-face">
    <span class="dot"></span>
    <span class="dot"></span>
</div>
<div class="three-face">
    <span class="dot"></span>
    <span class="dot"></span>
    <span class="dot"></span>
</div>
<div class="four-face">
    <div class="column">
        <span class="dot"></span>
        <span class="dot"></span>
    </div>
    <div class="column">
        <span class="dot"></span>
        <span class="dot"></span>
    </div>
</div>
<div class="five-face">
    <div class="column">
        <span class="dot"></span>
        <span class="dot"></span>
    </div>
    <div class="column">
        <span class="dot"></span>
    </div>
    <div class="column">
        <span class="dot"></span>
        <span class="dot"></span>
    </div>
</div>
<div class="six-face">
    <div class="column">
        <span class="dot"></span>
        <span class="dot"></span>
        <span class="dot"></span>
    </div>
    <div class="column">
        <span class="dot"></span>
        <span class="dot"></span>
        <span class="dot"></span>
    </div>
</div>
```
css代码：
```css
body {
    display: flex;
}
.dot {
    display: block;
    width: 24px;
    height: 24px;
    border-radius: 50%;
    background-color: #333;
    margin: 4px;
    box-shadow: inset 0 3px #111, inset 0 -3px #555;
}
[class$="face"] {
    width: 104px;
    height: 104px;
    margin: 16px;
    padding: 4px;
    background-color: #e7e7e7;
    border-radius: 10%;
    object-fit: contain;
    box-shadow:
        inset 0 5px white,
        inset 0 -5px #bbb,
        inset 5px 0 #d7d7d7,
        inset -5px 0 #d7d7d7;
}
.one-face {
    display: flex;
    justify-content: center;
    align-items: center;
}
.two-face {
    display: flex;
    justify-content: space-between;
}
.two-face .dot:nth-child(2) {
    align-self: flex-end;
}
.three-face {
    display: flex;
    align-items: center;
    justify-content: space-between;
}
.three-face .dot:nth-child(1) {
    align-self: flex-start;
}
.three-face .dot:nth-child(3) {
    align-self: flex-end;
}
.four-face {
    display: flex;
    justify-content: space-between;
}
.four-face .column {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
}
.five-face {
    display: flex;
    justify-content: space-between;
}
.five-face .column {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
}
.five-face .column:nth-child(2) {
    align-self: center;
}
.six-face{
    display: flex;
    flex-direction: column;
    justify-content: space-between;
}
.six-face .column{
    display: flex;
    justify-content: space-between;
}
```