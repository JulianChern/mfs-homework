## 有哪些水平居中的方法？请分别回答当元素为：行内元素，定宽块状元素时的方法和通用方法

* 行内元素：父元素中设置`text-align: center;`
* 定宽块状元素：设置`margin: 0 auto;`
* 不定宽块状元素：设置`display: inline;`然后当做行内元素处理
* 通用方案：使用flex布局，在父元素中设置：`display: flex; justify-content: center;`

## 有哪些垂直居中的方法？

* 单行内联文本：在父元素中设置`line-height`为其`height`
* 多行内联文本：在父元素中设置`display: table-cell`或者`display: inline-block`，再`vertical-align: middle`
* 块状元素：在子元素中设置`position: fixed`或`absolute`，然后设置`margin: auto`
* 通用方案：使用flex布局，给父元素设置`display: flex; align-items: center;`

## 单列布局是什么？

单列布局是指网页主要分为三部分：头部、内容、尾部，三部分的排成一列。主要有如下两种：

* 一种是header、content、footer宽度都相同，其一般不会占满浏览器的最宽宽度，但当浏览器宽度缩小低于其最大宽度时，宽度会自适应。
* 一种是header、footer宽度为浏览器宽度，但content以及header和footer里的内容却不会占满浏览器宽度。


## css 中允许负 margin 吗？什么时候需要使用负 margin？

允许
在圣杯布局和双飞翼布局等多列布局中会使用。

详情可参考:<https://www.cnblogs.com/xiaohuochai/p/5314289.html>

## 代码题

### 1.完全居中
```
body{
    padding: 0;
    margin:0;
}
[class*=container]{
    margin:5px;
    height: 200px;
    width:200px;
    background-color: #ccc;
    border: 1px solid red;
}
[class*=item]{
    height: 100px;
    width: 100px;
    border:1px dashed #555;
    background-color: #eee;
}
/*方案1 display:inline*/
.container0{
    line-height: 200px;
    text-align: center;
}
.item0{
    display: inline;
}
/*方案2 table-cell+vertical*/
.container1{
    display: table-cell;
    vertical-align: middle;
}
.item1{
    margin:0 auto;
}
/*方案3 flex布局*/
.container2{
    display: flex;
    justify-content: center;
    align-items: center;
}
.item2{
    text-align: center;
    line-height: 100px;
}
```

演示: <https://zfhxi.github.io/mfs-homework/css_base/ex21-%E5%B1%85%E4%B8%ADdemo.html>

### 2.传统圣杯布局

代码见<https://github.com/zfhxi/mfs-homework/blob/master/css_base/ex21-%E5%9C%A3%E6%9D%AF%E5%B8%83%E5%B1%80_float%E6%96%B9%E6%A1%88.html>

演示<https://zfhxi.github.io/mfs-homework/css_base/ex21-%E5%9C%A3%E6%9D%AF%E5%B8%83%E5%B1%80_float%E6%96%B9%E6%A1%88.html>

### 3.flex圣杯布局

代码见<https://github.com/zfhxi/mfs-homework/blob/master/css_base/ex21-%E5%9C%A3%E6%9D%AF%E5%B8%83%E5%B1%80_flex%E6%96%B9%E6%A1%88.html>

演示<https://zfhxi.github.io/mfs-homework/css_base/ex21-%E5%9C%A3%E6%9D%AF%E5%B8%83%E5%B1%80_flex%E6%96%B9%E6%A1%88.html>
