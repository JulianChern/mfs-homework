问答题
## 什么是 CSS？它有什么优势？

CSS 指层叠样式表 (Cascading Style Sheets)

其优势：

* 定义如何显示 HTML 元素
* 解决内容与表现分离的问题
* 极大提高工作效率
* 将多重样式将层叠为一个

## 内部样式表和外部样式表各是如何声明和使用？

内部样式表声明在&lt;head>中，如：
```html
<head>
    <style>
        h2 {
            color: red;
        }
    </style>
</head>
```

外部样式表是在&lt;head>中通过&lt;link>标签引入外部资源*.css文件声明，如：
```html
<head>
    <link rel="stylesheet" href="./style.css">
</head>
```
`style.css`
```css
h2 {
    color: red;
}
```

## 多层样式层叠是什么意思？

是指样式表允许以多种方式规定样式信息，在同一个 HTML 文档内部引用规定在对个地方的样式表。如，规定在单个的 HTML 元素中，在 HTML 页的头元素中，或在一个外部的 CSS 文件中的样式表。

## 什么是优先级？样式表的优先级排序是什么？

优先级指当同一个 HTML 元素被不止一个样式定义时，浏览器决定使用哪个样式的规则。

优先级顺序：

内联样式（在 HTML 元素内部）> 内部样式表（位于 &lt;head> 标签内部）> 外部样式表> 浏览器缺省设置

## 请解释 CSS 如下概念

* 选择器
* 属性
* 值
* 声明
* 声明块

`选择器（Selector）`: 选择需要应用样式的元素对象。

`属性（property）`:CSS1、CSS2、CSS3规定了许多的属性，通过设置属性的值达到控制选择器的样式的目的。

`值（value）`:指属性接受的设置值/关键字，一个属性的值要么是关键字、要么是一个关键字列表。如果一个属性可以取多个关键字，则多个关键字时大都以空格隔开，例如p{font:medium Helvetica}。

`声明（declaration）`:属性和值之间用半角冒号：隔开，形成一条声明。

`声明块(declaration block)`：多个特性间用；隔开，前后用{}括起来，形成一个声明块。


## CSS 中回车符(\n)会影响 CSS 的解析吗？请测试在不同位置下的回车符对解析的影响，然后谈谈你的理解

经过不完整初测，\n可加在CSS中能出现空格的地方。

示例：
```css
h2
{
    color
    :
     hotpink
     ;

    font-size

    :

    30px
    ;

    border
    :
    1px
    solid
    #f00
    ;
}
```

这也很好地反映css多行代码可以被后期处理为一行代码。

## CSS 代码中如何使用注释？

示例：
```css
/*
多行注释
*/
h2 {
    color:blue;/*单行注释*/
    margin:0;
    font-size: 20px;
}
```

## 浏览器前缀是什么？我们为什么需要浏览器前缀？

有些新式的CSS样式并不是所有的浏览器都支持，一些浏览器率先支持了，或者不是所有的浏览器支持情况都一样，我们需要前缀写法
```
Firefox: -moz-
Safari/Chrome: -webkit-
Opera: -o-
IE: -ms-
```

## 对浏览器前缀的处理有哪些方案？

处理方案如下：

* 若需要兼容旧版浏览器时，我们需要添加前缀。
* 当一个属性为标准属性时，不需要添加前缀。
* 当项目对浏览器兼容性有所要求时，如需要兼容95%的浏览器，可在caniuse.com上查看标签写前缀和未添加前缀时浏览器的兼容百分占比差距，来决定是否需要添加前缀。
* 在需要添加许多属性前缀的情况下，可使用 css 预处理器来完成。


## CSS 缩写是什么？哪些属性支持缩写？

css缩写是指可以将多个属性缩成一行，并只保留一个属性名。如：
```css
border-width: 1px;
border-style: solid;
border-color: #000;
```
可以缩写为一句：
```css
border: 1px solid #000;
```

支持缩写的属性如下：

* 颜色缩写：16进制的色彩值，如果每两位的值相同，可以进行缩写，例如： `＃000000`可以缩写为`#000`，`#336699`可以缩写为`#369`;

* 盒尺寸缩写：`Property: Value1 Value2 Value3 Value4;`四个值依次表示Top，Right，Bottom，Left

* 边框缩写 边框的属性如下：
```css
border-width: 1px;
border-style: solid;
border-color: #000;
```
可以缩写为一句：
```css
border: 1px solid #000;
```
* 背景缩写
```css
background-color: #F00;
background-image: url(background.gif);
background-repeat: no-repeat;
background-attachment: fixed;
background-position: 0 0;
```
可以缩写为一句：
```css
background: #F00 url(background.gif) no-repeat fixed 0 0;
```
* 文字缩写
```css
font-weight: bold;
font-size: 12px;
line-height: 1.8em;
font-family: Arial;
```
可以缩写为一句： 但文字缩写一定要具有字号、字体样式这两个属性。行高用/分隔
```css
font:bold 12px/1.8em Arial;
```

## 什么是 CSS 中的继承？哪些属性可以继承？如果不希望子元素继承父元素的某个属性该怎么办？

继承是指某些样式不仅能应用到指定的元素，还会应用到它的后代元素。

常用的可继承的元素：

* 文本相关属性
```
font-family, font-size, font-style,
font-variant, font-weight, font, letter-spacing,
line-height, text-align, text-indent, 
texttransform, word-spacing
```
* 列表相关属性
```
list-style-image, list-style-position,
list-style-type, list-style
```
还有一个属性比较重要，`color`属性。

不希望继承，那么就在子元素中重新指定即可。如：

不希望p标签继承body的字体，那么可：
```css
body {
  font-family: Verdana, sans-serif;
}
p {
  font-family: Times, "Times New Roman", serif;
}
```



## 以下代码<h2>最终显示成什么颜色？为什么？

```html
<!DOCTYPE html>
<html>

<head>
<style type="text/css">
  body{
    color: yellow;
  }
</style>
</head>

<body>
<h2>标题</h2>
</body>

</html>
```
显示黄色，因为内部样式优先级大于缺省设置。

## 以下代码中&lt;h2>最终显示成什么颜色？如果我想让color: red，在不修改内部和外部样式表的前提下，该如何实现？

`index.html`
```html
<!DOCTYPE html>
<html>

<head>
 <link rel="stylesheet" type="text/css" href="style.css">
 <style type="text/css">
   body{
     color: yellow;
   }
   h2 {
     color: blue;
   }
 </style>
</head>

<body>
 <h2>标题</h2>
</body>

</html>
```
`style.css`
```
h2 {
 color: green
}
```

显示蓝色。
如果不修改内部和外部样式来改变颜色，那么只有通过设定内联样式：`<h2 style="color:red"></h2>`